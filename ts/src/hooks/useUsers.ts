import axios from 'axios';

import { useQuery } from 'react-query';
import { User } from '../model/user';

export const useUsers = () => {
  const getUsers = async () => {
    const resp = await axios.get<User[]>('http://localhost:3001/users');
    return resp.data;
  }

  const userQuery = useQuery('users', getUsers);
  return userQuery;
};