import React from 'react';
import ReactDOM from 'react-dom';

import { Pages } from './pages';
import { GlobalStyles } from './styles';

ReactDOM.render(
  <React.StrictMode>
    <GlobalStyles />
    <Pages />
  </React.StrictMode>,
  document.getElementById('root')
);