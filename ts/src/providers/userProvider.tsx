import { FC, createContext, useState } from "react";
import { User } from '../model/user';

export const UserContext = createContext<User | null>(null);
export const UserSetContext = createContext<any>(null);

export const UserProvider: FC = ({children}: any) => {
  const [user, setUser] = useState<User | null>(null);
  return (
    <UserContext.Provider value={user}>
      <UserSetContext.Provider value={setUser}>
        {children}
      </UserSetContext.Provider>
    </UserContext.Provider>
  );
};
