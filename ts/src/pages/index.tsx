import { FC } from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { UserProvider } from "../providers/userProvider";
import { Footer } from "../components/footer";
import { Header } from "../components/header";
import { Home } from "./home";
import { ADChanges } from "./a-d-changes";

export type PagesParams = {};

export const Pages: FC<PagesParams> = () => {  
  const queryClient = new QueryClient();
  
    return (
      <QueryClientProvider client={queryClient}>
        <UserProvider>
          <Header />
          <Router>
            <Switch>
              <Route path={`/adchanges`}>
                <ADChanges />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </Router>
          <Footer />
          <ReactQueryDevtools initialIsOpen={true} />
        </UserProvider>
      </QueryClientProvider>
    );
  };