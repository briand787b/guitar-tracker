export type User = {
    id: number;
    firstName: string;
    lastName: string;
    friendIds: number[];
}