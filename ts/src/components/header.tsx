/** @jsxImportSource @emotion/react */
import { FC, useContext } from "react";
import { css } from "@emotion/react";
import { UserSetContext } from "../providers/userProvider";
import { useUsers } from "../hooks/useUsers";

export type HeaderProps = {};

export const Header: FC<HeaderProps> = ({}) => {
  const setUser = useContext(UserSetContext);
  
  const userResponse = useUsers()
  let users = userResponse.data || [];

  let errMsg = userResponse.error
  if (errMsg != null) {
    console.log('errMsg', errMsg)
  }
  
  return (
    <header
      css={css`
        padding: 32px;
        background-color: burlywood;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: flex-end;
      `}
    >
      <h1>Header</h1>
      <select
        onChange={(e) =>
          setUser(users.find((f) => `${f.id}` === e.target.value))
        }
        css={{
          marginLeft: "45%",
        }}
      >
        <option hidden>-- select user --</option>
        {users.map((f) => (
          <option key={f.id} value={f.id}>
            {f.firstName} {f.lastName}
          </option>
        ))}
      </select>
    </header>
  );
};
