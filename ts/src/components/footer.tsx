/** @jsxImportSource @emotion/react */
import { FC } from "react";
import { css } from "@emotion/react";

export type FooterProps = {};

export const Footer: FC<FooterProps> = ({}) => {
  return (
    <footer
      css={css`
        background-color: coral;
        padding: 32px;
      `}
    >
      <h3>Footer</h3>
    </footer>
  );
};